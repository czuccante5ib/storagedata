# Progetto applicazione per salvataggio dati su di un file testo (parte 1)

## Finalita'
L'applicazione si basa sull'utilizzo di activity e di intent espliciti e sulla 
scrittura e lettura da file

## Descrizione
L'app e' costituita da 2 activity. Nell'activity main deve essere 
realizzata una maschera che abbia le seguenti caratteristiche:

1. Un messaggio testuale che invita l'utente a registrare i propri dati
Subito al di sotto vanno inseriti:
2. un campo testo "nome" e un campo testo editabile dove andra' inserito il nome dell'utente
3. un campo testo "cognome" e un campo testo editabile dove andra' inserito il cognome dell'utente 
4. un campo testo "login" e un campo testo editabile dove andra' inserita la username
5. un campo testo "password" e un campo testo editabile dove andra' inserita la password (in chiaro)
6. un campo testo "email" e un campo testo editabile dove andra' inserito l'indirizzo di posta elettronica

Vanno inoltre inseriti 3 bottoni posizionati al di sotto dei campi.
Il primo bottone ha la funzione di salvare il contenuto dei campi editabili in un file testo (.txt). Nel momento in cui verra' premuto dovra' comparire un fumetto indicante il salvataggio avvenuto.
Il secondo bottone ha la funzione di cancellare il contenuto di tutti i campi editabili.
Il terzo bottone fa partire la seconda activity dove verra' visualizzato il contenuto del file in formato testo.

Nella seconda activity inoltre deve essere previsto un ulteriore bottone per il ritorno all'activity main.

Il colore dei bottoni puo' essere o grigio chiaro o azzurro chiaro.

## Osservazioni
Avviare android studio: Start new Android Studio project > Empty Activity. Per realizzare l'app saranno necessari i widget **Button**, **TextView** e **EditText**.  
Nel file main_activity.java vanno dunque implementati 3 metodi: uno per scrivere il testo sul file, uno per cancellare i campi da completare e infine uno per leggere il testo sul file. 

Si forniscono delle tracce di codice per raggiungere l'obiettivo. 
Android offre il metodo openFileOutput() per salvare un file:

```
FileOutputStream fileOut=openFileOutput("filename.txt", Context.MODE_PRIVATE);
            OutputStreamWriter outputWriter=new OutputStreamWriter(fileOut);
            outputWriter.write(textmsg);
            outputWriter.close();
    }
```
Android offre il metodo openFileInput() per leggere un file:
```
int READ_BLOCK_SIZE=100;
....
FileInputStream fileInp=openFileInput("filename.txt");
InputStreamReader InputRead= new InputStreamReader(fileInp);
            char[] inBuffer= new char[READ_BLOCK_SIZE];
            String s="";
            int chread;
            while ((chread=InputRead.read(inBuffer))>0) {
            // char to string conversion
            String readstring=String.copyValueOf(inBuffer,0,charRead);
            s +=readstring;
            }
InputRead.close();
```
La documentazione di riferimento la si puo' trovare [qui](https://developer.android.com/reference/java/io/FileOutputStream) e [qui](https://developer.android.com/reference/java/io/FileInputStream)

# Progetto applicazione per salvataggio e lettura dati su di un file testo (parte 2)

## Procedura
Al layout principale si dovra' aggiungere un terzo bottone [Read] che dovra' richiamare una seconda activity. Nell'activity figlia dovra' essere stampato il contenuto del file creato con il metodo openFileOutput e inoltre dovra' essere presente un bottone in basso [Return] che fa ritornare il controllo all'activity main.

Si ricorda che per la realizzazione di questa parte diventa neccessario l'utilizzo degli intent espliciti.
Anche in questo caso  saranno necessari i widget **Button**, **TextView**.
Le dimensioni dei font per la visualizzazione del contenuto vanno scelte tra 15dp - 25dp comunque non esageratamente grandi.

 

